# [0.5.0](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.4.2...v0.5.0) (2021-05-17)


### Features

* add check for job output check ([7496f1a](https://gitlab.com/blissfulreboot/python/pettifogger/commit/7496f1ac8ecd98608491f248ba6f99cdb238480c))

## [0.4.2](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.4.1...v0.4.2) (2021-05-17)


### Bug Fixes

* catch unknown exceptions ([0c72b3e](https://gitlab.com/blissfulreboot/python/pettifogger/commit/0c72b3e033a961accfc95bff8c619460f614308f))

## [0.4.1](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.4.0...v0.4.1) (2021-05-10)


### Bug Fixes

* undefined needs check missing return and exception from schema validation uncaught ([90b9b4f](https://gitlab.com/blissfulreboot/python/pettifogger/commit/90b9b4f5f24b9f824bf327acf471ccae75c73861))
* **main:** catch yaml load exceptions ([8a12646](https://gitlab.com/blissfulreboot/python/pettifogger/commit/8a1264689ccf5ac470248010281c99135a69ce5b))
* **structure/root:** schema error path is printed incorrectly ([ea1c925](https://gitlab.com/blissfulreboot/python/pettifogger/commit/ea1c925b997d33584395942d2197693871d8f049))

# [0.4.0](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.3.2...v0.4.0) (2021-05-10)


### Features

* add github actions built-in environment variables ([1106dae](https://gitlab.com/blissfulreboot/python/pettifogger/commit/1106daeeb3c0efe95b9311571eda591241c26fb3))

## [0.3.2](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.3.1...v0.3.2) (2021-05-10)


### Bug Fixes

* schema path part can be integer ([4a0eb2a](https://gitlab.com/blissfulreboot/python/pettifogger/commit/4a0eb2a24fba9972c0e1fa5acc2648b706c613db))

## [0.3.1](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.3.0...v0.3.1) (2021-05-09)


### Bug Fixes

* **.gitlab-ci.yml:** pass changed files as artifacts in CI and release corrected package ([d0dcf45](https://gitlab.com/blissfulreboot/python/pettifogger/commit/d0dcf45bdc55a4726444654b1cefdf059a38892b))

# [0.3.0](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.8...v0.3.0) (2021-05-09)


### Bug Fixes

* correct the --watch argument description ([daca76a](https://gitlab.com/blissfulreboot/python/pettifogger/commit/daca76a162517b85b6def6606b7417c6614f62b1))
* improve readability of logs, fix log state bug ([c3de680](https://gitlab.com/blissfulreboot/python/pettifogger/commit/c3de6806559bbadbff5a479111c76cce70eedd77))


### Features

* finish watch implementation ([6c97641](https://gitlab.com/blissfulreboot/python/pettifogger/commit/6c9764118898f287b2847e8f3ba0a7a2efd03ee5)), closes [#3](https://gitlab.com/blissfulreboot/python/pettifogger/issues/3)

## [0.2.8](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.7...v0.2.8) (2021-05-07)


### Bug Fixes

* remove debug print ([25f60f1](https://gitlab.com/blissfulreboot/python/pettifogger/commit/25f60f1556989cd230adf4a9963d4f9181f4e4f9))

## [0.2.7](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.6...v0.2.7) (2021-05-07)


### Bug Fixes

* use current working directory instead of main file's directory ([53938c9](https://gitlab.com/blissfulreboot/python/pettifogger/commit/53938c9a7c712788a53f16bb1bdb498b5b394050))

## [0.2.6](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.5...v0.2.6) (2021-05-06)


### Bug Fixes

* credentials fix ([686c1e9](https://gitlab.com/blissfulreboot/python/pettifogger/commit/686c1e9f0b1b1ed0282ea351e464245b2a948699))

## [0.2.5](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.4...v0.2.5) (2021-05-06)


### Bug Fixes

* credentials fix ([9e36b33](https://gitlab.com/blissfulreboot/python/pettifogger/commit/9e36b330f57c3af421bda87016ff4ba55bd6a780))

## [0.2.4](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.3...v0.2.4) (2021-05-06)


### Bug Fixes

* credentials fix ([51c7064](https://gitlab.com/blissfulreboot/python/pettifogger/commit/51c706495b97300a74bdbcc0fabee8b418c4346d))

## [0.2.3](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.2...v0.2.3) (2021-05-06)


### Bug Fixes

* credentials fix ([8dfd5d7](https://gitlab.com/blissfulreboot/python/pettifogger/commit/8dfd5d7ca66ba6e03af4be94f971882b20622bbe))

## [0.2.2](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.1...v0.2.2) (2021-05-06)


### Bug Fixes

* build and publish command fix and version bump ([cce6135](https://gitlab.com/blissfulreboot/python/pettifogger/commit/cce613581af99e95bafd1c9a907ad69628ec6c63))

## [0.2.1](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.2.0...v0.2.1) (2021-05-06)


### Bug Fixes

* ci fixes and forced version bump ([c61aec7](https://gitlab.com/blissfulreboot/python/pettifogger/commit/c61aec799edc53f9e7d3fadc5ead8aefc83c6974))

# [0.2.0](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.1.4...v0.2.0) (2021-05-06)


### Features

* mostly rewritten codebase ([a25a9ce](https://gitlab.com/blissfulreboot/python/pettifogger/commit/a25a9ce1f7768c1e25728aab1feb380762cc21a7))

## [0.1.4](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.1.3...v0.1.4) (2021-05-03)


### Bug Fixes

* correct imports in pettifogger.py ([5b2d080](https://gitlab.com/blissfulreboot/python/pettifogger/commit/5b2d0809131d19ba3db5c82f089352da8c9dabaf))

## [0.1.3](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.1.2...v0.1.3) (2021-05-03)


### Bug Fixes

* *sigh* ...and some more... when will this end... ([af664d3](https://gitlab.com/blissfulreboot/python/pettifogger/commit/af664d3b5cd2f8a32527c39d08c93a111f4cc39d))
* *sigh* ...and some more... when will this end... ([ebb471a](https://gitlab.com/blissfulreboot/python/pettifogger/commit/ebb471a018e41da4819125df25d4ec694ff77018))
* *sigh* ...GROAAAAAAAANNNNN ([936a50b](https://gitlab.com/blissfulreboot/python/pettifogger/commit/936a50bfea2e8fe4358c03ee901a0ab9ace83d5b))
* *sigh* ...GROAAAAAAAANNNNNgggh ([ae1ca45](https://gitlab.com/blissfulreboot/python/pettifogger/commit/ae1ca45496f05dc583c40e0a7c60ea288343749f))
* *sigh* ...GROAAAAAAAANNNNNgggh ([6e77d87](https://gitlab.com/blissfulreboot/python/pettifogger/commit/6e77d87487be7f2fdd00f823c2a4b78fe58b362f))
* *sigh* ...GROAAAAAAAANNNNNgggh ([7359fe5](https://gitlab.com/blissfulreboot/python/pettifogger/commit/7359fe55f797a3ee9a73ec56208f6e0ac1e891b0))
* *sigh* ...never... it... never.... ends ([f914ac9](https://gitlab.com/blissfulreboot/python/pettifogger/commit/f914ac91228294338a2a6b59a8067b8c8e92268d))
* fixing various env variable detection and parsing errors ([f738776](https://gitlab.com/blissfulreboot/python/pettifogger/commit/f738776bd547667e494a405fcfeec05acfbd7497))
* **chekout check:** correct typo in the action name ([9e73eb3](https://gitlab.com/blissfulreboot/python/pettifogger/commit/9e73eb337a8ff44e4eff9f69c631151f9447f21c))
* **schema check:** use correct definition reference for env ([ec94513](https://gitlab.com/blissfulreboot/python/pettifogger/commit/ec94513b69c12c94b74064f5e9c5d8664123ebb7))

## [0.1.2](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.1.1...v0.1.2) (2021-05-03)


### Bug Fixes

* transition to poetry from flit, script name fix ([853511d](https://gitlab.com/blissfulreboot/python/pettifogger/commit/853511d6a8098d32d2e03c9b02f66dcc5bf7e16b))

## [0.1.1](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.1.0...v0.1.1) (2021-05-02)


### Bug Fixes

* fix the entry script ([82f152f](https://gitlab.com/blissfulreboot/python/pettifogger/commit/82f152fe96f3ea5a02618fb53dcdc3fd23f5736b))

# [0.1.0](https://gitlab.com/blissfulreboot/python/pettifogger/compare/v0.0.0...v0.1.0) (2021-05-02)


### Features

* added checks for needs, fixes to pipelines ([69ab568](https://gitlab.com/blissfulreboot/python/pettifogger/commit/69ab568492871891cf363071e61b8df62202bd61))
* initial version for testing ([e8bfb00](https://gitlab.com/blissfulreboot/python/pettifogger/commit/e8bfb003532cb19d61a964b0fa974d42ca077760))
